Using avr-gcc compilers with avr headers
========================================

Uses register definitions from <avr/io.h>,
not from any Arduino stuff.

Good:
- Done on command line.
- Does not rely on Arduino development environment.
- Does not rely on files from Arduino.
- Does not rely on libraries from Arduino.

Bad:
- Many names lack meaning that explains.
- Names from <avr/io.h> scream in uppercase.
- wait() is uncalibrated.
- Does not use TDD.
