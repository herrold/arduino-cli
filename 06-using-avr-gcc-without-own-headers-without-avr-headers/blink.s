	.file	"blink.c"
__SP_H__ = 0x3e
__SP_L__ = 0x3d
__SREG__ = 0x3f
__tmp_reg__ = 0
__zero_reg__ = 1
	.text
.global	wait
	.type	wait, @function
wait:
	push r16
	push r17
	push r28
	push r29
	rcall .
	rcall .
	in r28,__SP_L__
	in r29,__SP_H__
/* prologue: function */
/* frame size = 4 */
/* stack size = 8 */
.L__stack_usage = 8
	std Y+1,__zero_reg__
	std Y+2,__zero_reg__
	std Y+3,__zero_reg__
	std Y+4,__zero_reg__
.L2:
	ldd r16,Y+1
	ldd r17,Y+2
	ldd r18,Y+3
	ldd r19,Y+4
	cp r16,r22
	cpc r17,r23
	cpc r18,r24
	cpc r19,r25
	brsh .L5
	ldd r16,Y+1
	ldd r17,Y+2
	ldd r18,Y+3
	ldd r19,Y+4
	subi r16,-1
	sbci r17,-1
	sbci r18,-1
	sbci r19,-1
	std Y+1,r16
	std Y+2,r17
	std Y+3,r18
	std Y+4,r19
	rjmp .L2
.L5:
/* epilogue start */
	pop __tmp_reg__
	pop __tmp_reg__
	pop __tmp_reg__
	pop __tmp_reg__
	pop r29
	pop r28
	pop r17
	pop r16
	ret
	.size	wait, .-wait
	.section	.text.startup,"ax",@progbits
.global	main
	.type	main, @function
main:
/* prologue: function */
/* frame size = 0 */
/* stack size = 0 */
.L__stack_usage = 0
	sbi 0x4,5
.L7:
	sbi 0x5,5
	ldi r22,lo8(-96)
	ldi r23,lo8(-122)
	ldi r24,lo8(1)
	ldi r25,0
	call wait
	cbi 0x5,5
	ldi r22,lo8(-96)
	ldi r23,lo8(-122)
	ldi r24,lo8(1)
	ldi r25,0
	call wait
	rjmp .L7
	.size	main, .-main
	.ident	"GCC: (GNU) 5.4.0"
