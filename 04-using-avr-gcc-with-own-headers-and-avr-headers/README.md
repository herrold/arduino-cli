Move register definitions into separate header files
====================================================

This is about improving the structure of the program.
Having almost everything in one file is an ugly mess,
so register definitions are moved into separate header files.

    -rw-r-----  1 jep  staff   286 Feb  5 21:06 arduino-uno-r3.h
    -rw-r-----  1 jep  staff   768 Feb  5 21:06 atmega328.h

atmega328.h (re)defines registers of the ATmega328* microcontroller chips
without anything specific to boards using those chips.

arduino-uno-r3.h (re)defines registers specific to use on Arduino UNO r3
boards.

Good:
-   Improved modularity and file structure.
-   Done on command line.
-   Does not rely on Arduino development environment.

Bad:
-   wait() is uncalibrated.
-   Does not use TDD.
