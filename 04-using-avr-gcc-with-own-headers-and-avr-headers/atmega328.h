#include <avr/io.h>

/*
* register definitions for AVR ATmega328P microcontroller from
* http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
*
* $ ll ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* -rw--w----@ 1 jep  staff  34343538 Nov  1  2018 ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* $ md5sum ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* 9936a914b2398ce4c9deb076627fda28 ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* $
*/

/* use lower-case names for mutable and volatile things */

#define ddrb (DDRB)
#define portb (PORTB)
#define pinb (PINB)

/* more meaningful names that are not standard */

#define portb_output_direction (ddrb)
#define portb_output (portb)
#define portb_input (pinb)
