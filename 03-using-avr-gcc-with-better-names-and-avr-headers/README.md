Use more meaningful names
=========================

Good:
-   Code is easier to understand because more meaningful names are used.
-   Done on command line.
-   Does not rely on Arduino development environment.
-   Provenance of datasheet is documented.

Bad:
-   Too much stuff in the C file.
-   wait() is uncalibrated.
-   Does not use TDD.
