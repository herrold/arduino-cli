#include <stdint.h>
#include <avr/io.h>

/*
* register definitions for AVR ATmega328P microcontroller from
* http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
*
* $ ll ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* -rw--w----@ 1 jep  staff  34343538 Nov  1  2018 ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* $ md5sum ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* 9936a914b2398ce4c9deb076627fda28 ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf
* $
*/

/* lower-case names for mutable things */

#define ddrb (DDRB)
#define portb (PORTB)
#define pinb (PINB)

/* more meaningful names for chip stuff that are not standard */

#define portb_output_direction (ddrb)
#define portb_output (portb)
#define portb_input (pinb)

/* meaningful names for stuff particular to Arduino UNO r3 board */

#define led_output_direction (portb_output_direction)
#define led_output_direction_mask (1<<5)
#define led_output_port (portb_output)
#define led_on_mask (1<<5)

void wait(const uint32_t n)
{
    for (volatile uint32_t i = 0; i < n; i++)
        ;
}

int main(void)
{
    led_output_direction |= led_output_direction_mask;

    for (;;) {
        led_output_port |=  led_on_mask;
        wait(100000U);
        led_output_port &= ~led_on_mask;
        wait(100000U);
    }
}
